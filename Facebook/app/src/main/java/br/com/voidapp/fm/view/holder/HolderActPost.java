package br.com.voidapp.fm.view.holder;


import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.EditText;

import br.com.voidapp.fm.R;
import br.com.voidapp.fm.view.ISupportView;

public class HolderActPost {

    private final ISupportView mView;

    private Toolbar mToolbar;

    private EditText mHowFeelingText;
    private Button mSendButton;

    public HolderActPost(final ISupportView view) {
        mView = view;
        initializeFields();
    }

    private void initializeFields() {
        mToolbar = (Toolbar) mView.findViewById(R.id.act_post_toolbar);
        mHowFeelingText = (EditText) mView.findViewById(R.id.act_post_input_layout);
        mSendButton = (Button) mView.findViewById(R.id.act_post_bt_send);
    }

    public EditText getHowFeelingText() {
        return mHowFeelingText;
    }

    public Toolbar getToolbar() {
        return mToolbar;
    }

    public Button getSendButton() {
        return mSendButton;
    }
}
