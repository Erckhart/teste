package br.com.voidapp.fm.view.activity;

import android.support.v7.app.AppCompatActivity;

import br.com.voidapp.fm.FMApp;
import br.com.voidapp.fm.view.ISupportView;

public abstract class FbActivity extends AppCompatActivity implements ISupportView {

    @Override
    public FMApp getApp() {
        return (FMApp) getApplication();
    }
}
