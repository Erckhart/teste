package br.com.voidapp.fm.controller;

import android.os.Handler;
import android.view.View;

import com.facebook.Profile;

import br.com.voidapp.fm.view.ISupportFragView;
import br.com.voidapp.fm.view.holder.HolderFragProfile;

public class ControllerFragProfile {

    private final Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            mHolder.putValues(Profile.getCurrentProfile());
            mHolder.setLoadingEnabled(false);
        }
    };

    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(final View v) {
            mView.getSupportView().getApp().getFacebookManager().logout();
            mView.getSupportView().finish();
        }
    };


    private final ISupportFragView mView;
    private final HolderFragProfile mHolder;


    public ControllerFragProfile(final ISupportFragView view) {
        mView = view;
        mHolder = new HolderFragProfile(mView);
        initialize();
        initializeActions();
    }

    private void initialize() {
        mHolder.setLoadingEnabled(true);
        Profile.fetchProfileForCurrentAccessToken();
        new Handler().postDelayed(mRunnable, 1000);
    }

    private void initializeActions() {
        mHolder.getLogoutButton().setOnClickListener(mOnClickListener);
    }

}
