package br.com.voidapp.fm.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import br.com.voidapp.fm.FMApp;
import br.com.voidapp.fm.R;
import br.com.voidapp.fm.controller.ControllerActMain;
import br.com.voidapp.fm.view.ISupportView;

public class ActMain extends FbActivity {

    private ControllerActMain mController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_main);
        mController = new ControllerActMain(this);
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        mController.onActivityResult(requestCode, resultCode, data);
    }
}
