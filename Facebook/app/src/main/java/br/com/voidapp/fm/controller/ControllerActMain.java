package br.com.voidapp.fm.controller;

import android.app.Activity;
import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.View;

import br.com.voidapp.fm.model.EFbMenu;
import br.com.voidapp.fm.view.IFragPager;
import br.com.voidapp.fm.view.ISupportView;
import br.com.voidapp.fm.view.activity.ActPost;
import br.com.voidapp.fm.view.adapter.AdpMenu;
import br.com.voidapp.fm.view.holder.HolderActMain;

public class ControllerActMain {

    public static final int REQUEST_POST_CODE = 1001;

    private final ViewPager.OnPageChangeListener mOnPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(final int position, final float positionOffset, final int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(final int position) {
            setupMenu(mAdpMenu.getMenu(position));
            ((IFragPager) mAdpMenu.getItem(position)).onFragmentSelected();
        }

        @Override
        public void onPageScrollStateChanged(final int state) {

        }
    };

    private final View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(final View v) {
            mView.startActivityForResult(new Intent(mView.getBaseContext(), ActPost.class), REQUEST_POST_CODE);
        }
    };


    private final ISupportView mView;
    private final HolderActMain mHolder;
    private final AdpMenu mAdpMenu;

    public ControllerActMain(final ISupportView view) {
        mView = view;
        mHolder = new HolderActMain(mView);
        mAdpMenu = new AdpMenu(mView.getSupportFragmentManager());
        initialize();
        initializeActions();
    }

    private void initialize() {
        mHolder.getViewPager().setAdapter(mAdpMenu);
        mHolder.getTabManager().setupWithViewPager(mHolder.getViewPager());
        setupMenu(EFbMenu.FEED);

        mView.setSupportActionBar(mHolder.getToolbar());

    }

    private void initializeActions() {
        mHolder.getViewPager().addOnPageChangeListener(mOnPageChangeListener);
        mHolder.getNewPostButton().setOnClickListener(mOnClickListener);
    }

    private void setupMenu(final EFbMenu menu) {
        final TabLayout.Tab feedTab = mHolder.getTabManager().getTabAt(0);
        final TabLayout.Tab profileTab = mHolder.getTabManager().getTabAt(1);
        menu.changeMenu(feedTab, profileTab);

        if (menu == EFbMenu.FEED) {
            mHolder.getNewPostButton().show();
        } else {
            mHolder.getNewPostButton().hide();
        }

    }

    public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        if(requestCode == REQUEST_POST_CODE && resultCode == Activity.RESULT_OK) {
            ((IFragPager)mAdpMenu.getItem(0)).onFragmentSelected();
        }
    }
}
