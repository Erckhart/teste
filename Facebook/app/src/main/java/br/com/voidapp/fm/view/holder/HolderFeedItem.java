package br.com.voidapp.fm.view.holder;

import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;

import br.com.voidapp.fm.R;
import br.com.voidapp.fm.model.Post;


public class HolderFeedItem extends RecyclerView.ViewHolder {

    private TextView mTitle;
    private TextView mContent;
    private ImageView mPostImage;

    private MapView mMapView;
    private GoogleMap mMaps;
    private Marker mPositionMarker;

    private final OnMapReadyCallback mOnMapReadyListener = new OnMapReadyCallback() {
        @Override
        public void onMapReady(final GoogleMap googleMap) {
            mMaps = googleMap;
            mMaps.setBuildingsEnabled(false);
            mMaps.setIndoorEnabled(false);
            mMaps.getUiSettings().setAllGesturesEnabled(false);
            mMaps.getUiSettings().setMapToolbarEnabled(false);

            final MarkerOptions options = new MarkerOptions();
            options.visible(false);
            options.position(new LatLng(0, 0));
            options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));

            mPositionMarker = mMaps.addMarker(options);

        }
    };

    public HolderFeedItem(final View view) {
        super(view);
        initializeFields();
    }

    private void initializeFields() {
        mTitle = (TextView) itemView.findViewById(R.id.frag_feed_item_post_title);
        mContent = (TextView) itemView.findViewById(R.id.frag_feed_item_post_content);

        mPostImage = (ImageView) itemView.findViewById(R.id.frag_feed_item_picture);
        mMapView = (MapView) itemView.findViewById(R.id.frag_feed_item_place);
        mMapView.getMapAsync(mOnMapReadyListener);
        mMapView.onCreate(null);
    }

    public void putValues(final Post value) {
        if (value != null) {
            mTitle.setText(value.getStory());
            mContent.setText(value.getMessage());

            if (TextUtils.isEmpty(value.getPicture())) {
                mPostImage.setImageResource(0);
                mPostImage.setVisibility(View.GONE);
                setMapPosition(value);
            } else {
                mMapView.setVisibility(View.GONE);
                mPostImage.setVisibility(View.VISIBLE);
                Glide.with(itemView.getContext()).load(Uri.parse(value.getPicture())).asBitmap().into(mPostImage);
            }
        }
    }

    private void setMapPosition(final Post value) {
        if (value.getPlace() == null) {
            mMapView.setVisibility(View.GONE);
            if (mPositionMarker != null) {
                mPositionMarker.setVisible(false);
            }
        } else if (mPositionMarker != null) {
            mMapView.setVisibility(View.VISIBLE);
            mPositionMarker.setVisible(true);
            mPositionMarker.setPosition(value.getPlace().getLocation().getPosition());
            if(mMaps != null) {
                mMaps.animateCamera(CameraUpdateFactory.newLatLngZoom(mPositionMarker.getPosition(), 15F));
            }
        }
    }

}
