package br.com.voidapp.fm.view.holder;

import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;

import br.com.voidapp.fm.R;
import br.com.voidapp.fm.view.ISupportFragView;
import br.com.voidapp.fm.view.widget.BottomRefreshRecyclerView;

public class HolderFragFeed {

    private final ISupportFragView mView;
    private BottomRefreshRecyclerView mFeedView;
    private SwipeRefreshLayout mSwipeToRefresh;

    public HolderFragFeed(final ISupportFragView view) {
        mView = view;
        initializeFields();
    }

    private void initializeFields() {
        mSwipeToRefresh = (SwipeRefreshLayout) mView.getView();
        mSwipeToRefresh.setColorSchemeColors(R.color.colorAccent, R.color.colorPrimary, R.color.colorPrimaryDark);
        mFeedView = (BottomRefreshRecyclerView) mView.getView().findViewById(R.id.frag_feed_list);
    }

    public SwipeRefreshLayout getSwipeToRefresh() {
        return mSwipeToRefresh;
    }

    public BottomRefreshRecyclerView getFeedView() {
        return mFeedView;
    }

}
