package br.com.voidapp.fm.controller.social;

import android.content.Context;
import android.os.Bundle;

import com.facebook.AccessToken;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.facebook.share.ShareApi;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareContent;
import com.facebook.share.model.ShareLinkContent;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.List;

import br.com.voidapp.fm.controller.util.ErrorUtil;
import br.com.voidapp.fm.model.Post;

public class SocialFacebook {

    public static final String[] PUBLISH_PERMISSIONS = {"publish_actions, user_posts"};

    private static final String FEED_GRAPH_REQUEST = "/%1$s/feed";
    private static final String FEED_FIELDS = "id,story,message,created_time,picture,place{name,location{latitude,longitude}}";

    public SocialFacebook(final Context context) {
        FacebookSdk.sdkInitialize(context);
    }

    public boolean isLoggedIn() {
        return AccessToken.getCurrentAccessToken() != null;
    }

    public void requestUserTimeline(final String userID, final OnUserTimelineLoadListener listener) {
        if (isLoggedIn()) {
            final GraphRequest request = new GraphRequest();
            final String endPoint = String.format(FEED_GRAPH_REQUEST, userID);
            request.setAccessToken(AccessToken.getCurrentAccessToken());
            request.setGraphPath(endPoint);
            request.setHttpMethod(HttpMethod.GET);

            final Bundle params = new Bundle();
            params.putString("fields", FEED_FIELDS);

            request.setParameters(params);
            requestUserTimeline(userID, request, listener);
        }
    }

    public void requestUserTimeline(final String userID, final GraphRequest request, final OnUserTimelineLoadListener listener) {
        final GraphRequest.Callback _RequestUserTimeLineCallback = new GraphRequest.Callback() {
            @Override
            public void onCompleted(final GraphResponse response) {
                if (listener != null) {
                    if (response.getError() == null) {
                        try {
                            final String responseJSON = response.getJSONObject().getString("data");
                            final Type type = new TypeToken<List<Post>>() {
                            }.getType();
                            final List<Post> posts = new Gson().fromJson(responseJSON, type);
                            listener.onTimelineRetrieved(userID, posts, response.getRequestForPagedResults(GraphResponse.PagingDirection.NEXT));
                        } catch (JSONException e) {
                            listener.onTimelineError("InternalError", e.getMessage());
                        }
                    } else {
                        listener.onTimelineError(response.getError().getErrorUserTitle(), ErrorUtil.getErrorContent(response.getError()));
                    }
                }
            }
        };
        request.setCallback(_RequestUserTimeLineCallback);
        request.executeAsync();
    }

    public void logout() {
        LoginManager.getInstance().logOut();
    }

    public void post(final String userID, final String message, final OnUserShareListener listener) {

        final String endPoint = String.format(FEED_GRAPH_REQUEST, userID);

        final Bundle params = new Bundle();
        params.putString("message", message);

        final GraphRequest request = new GraphRequest();
        request.setGraphPath(endPoint);
        request.setParameters(params);
        request.setAccessToken(AccessToken.getCurrentAccessToken());
        request.setHttpMethod(HttpMethod.POST);
        request.setCallback(new GraphRequest.Callback() {
            @Override
            public void onCompleted(final GraphResponse response) {
                if (listener != null) {
                    if (response.getError() == null) {
                        listener.onSuccessfulSharing();
                    } else {
                        listener.onError(ErrorUtil.getErrorContent(response.getError()));
                    }
                }
            }
        });
        request.executeAsync();

    }

    public interface OnUserShareListener {
        void onSuccessfulSharing();

        void onError(final String content);
    }

    public interface OnUserTimelineLoadListener {
        void onTimelineRetrieved(final String userID, final List<Post> posts, final GraphRequest nextPaging);

        void onTimelineError(final String title, final String content);
    }

}
