package br.com.voidapp.fm;

import android.app.Application;

import br.com.voidapp.fm.controller.social.SocialFacebook;

public class FMApp extends Application {

    private SocialFacebook mFacebookManager;

    @Override
    public void onCreate() {
        super.onCreate();
        mFacebookManager = new SocialFacebook(this);
    }

    public SocialFacebook getFacebookManager() {
        return mFacebookManager;
    }
}
