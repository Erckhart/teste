package br.com.voidapp.fm.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import br.com.voidapp.fm.R;
import br.com.voidapp.fm.controller.ControllerActLogin;
import br.com.voidapp.fm.view.ISupportView;

public class ActLogin extends FbActivity {

    private ControllerActLogin mController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_login);
        mController = new ControllerActLogin(this);
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mController.onActivityResult(requestCode, resultCode, data);
    }
}
