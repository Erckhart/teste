package br.com.voidapp.fm.view.fragment;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.request.Request;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;

import org.json.JSONObject;

import br.com.voidapp.fm.R;
import br.com.voidapp.fm.controller.ControllerFragProfile;
import br.com.voidapp.fm.view.IFragPager;
import br.com.voidapp.fm.view.ISupportView;

public class FragProfile extends Fragment implements IFragPager {


    private ControllerFragProfile mController;
    private ISupportView mSupportView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.frag_profile, container, false);
    }

    @Override
    public void onViewCreated(final View view, final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mController = new ControllerFragProfile(this);

    }

    @Override
    public void onAttach(final Context context) {
        super.onAttach(context);
        if (context instanceof ISupportView) {
            mSupportView = (ISupportView) context;
        }
    }

    @Override
    public void onAttach(final Activity activity) {
        super.onAttach(activity);
        if (activity instanceof ISupportView) {
            mSupportView = (ISupportView) activity;
        }
    }

    @Override
    public void onFragmentSelected() {

    }

    @Override
    public ISupportView getSupportView() {
        return mSupportView;
    }
}
