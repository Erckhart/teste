package br.com.voidapp.fm.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Post implements Serializable {

    private String id;

    private String story;

    private String message;

    @SerializedName("created_time")
    private String createdTime;

    private String picture;

    private Place place;

    public String getId() {
        return id;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public String getStory() {
        return story;
    }

    public String getMessage() {
        return message;
    }


    public String getPicture() {
        return picture;
    }

    public Place getPlace() {
        return place;
    }
}
