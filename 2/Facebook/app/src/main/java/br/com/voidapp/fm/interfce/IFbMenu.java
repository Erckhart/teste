package br.com.voidapp.fm.interfce;

import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;

public interface IFbMenu {

    void changeMenu(@NonNull final TabLayout.Tab feedTab,@NonNull final TabLayout.Tab profileTab);

}
