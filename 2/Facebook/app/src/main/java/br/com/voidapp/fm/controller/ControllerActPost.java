package br.com.voidapp.fm.controller;

import android.app.Activity;
import android.support.design.widget.Snackbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;

import com.afollestad.materialdialogs.MaterialDialog;
import com.facebook.AccessToken;

import br.com.voidapp.fm.R;
import br.com.voidapp.fm.controller.social.SocialFacebook;
import br.com.voidapp.fm.view.ISupportView;
import br.com.voidapp.fm.view.activity.FbActivity;
import br.com.voidapp.fm.view.holder.HolderActPost;

public class ControllerActPost {

    private final SocialFacebook.OnUserShareListener mOnUserShareListener = new SocialFacebook.OnUserShareListener() {
        @Override
        public void onSuccessfulSharing() {
            mLoadingDialog.dismiss();
            mView.setResult(Activity.RESULT_OK, null);
            mView.finish();
        }

        @Override
        public void onError(final String content) {
            mLoadingDialog.dismiss();
            Snackbar.make(mHolder.getSendButton(), content, Snackbar.LENGTH_SHORT).show();
        }
    };

    private final View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(final View v) {
            final String message = mHolder.getHowFeelingText().getText().toString();
            if (TextUtils.isEmpty(message)) {
                Snackbar.make(mHolder.getSendButton(), R.string.error_how_feeling_empty, Snackbar.LENGTH_SHORT).show();
            } else {
                mLoadingDialog.show();
                mFbManager.post(AccessToken.getCurrentAccessToken().getUserId(), message, mOnUserShareListener);
            }
        }
    };


    private final ISupportView mView;
    private final HolderActPost mHolder;
    private final SocialFacebook mFbManager;

    private MaterialDialog mLoadingDialog;

    public ControllerActPost(final ISupportView view) {
        mView = view;
        mHolder = new HolderActPost(mView);
        mFbManager = mView.getApp().getFacebookManager();
        initialize();
        initializeActions();
    }

    private void initialize() {
        mView.setSupportActionBar(mHolder.getToolbar());
        mView.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mLoadingDialog = new MaterialDialog.Builder((FbActivity) mView).progress(true, -1).content(R.string.lbl_sending).build();
    }

    private void initializeActions() {
        mHolder.getSendButton().setOnClickListener(mOnClickListener);
    }

    public boolean onOptionsItemSelected(final MenuItem item) {
        if(item.getItemId() == android.R.id.home) {
            mView.onBackPressed();
            return true;
        }
        return false;
    }
}
