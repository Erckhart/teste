package br.com.voidapp.fm.view;


public interface IFragPager extends ISupportFragView {

    void onFragmentSelected();

}
