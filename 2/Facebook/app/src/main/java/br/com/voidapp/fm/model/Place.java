package br.com.voidapp.fm.model;

public class Place {

    private String id;

    private Location location;

    private String name;

    public String getId() {
        return id;
    }

    public Location getLocation() {
        return location;
    }

    public String getName() {
        return name;
    }
}
