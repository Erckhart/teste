package br.com.voidapp.fm.view;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.StringRes;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.app.Fragment;
import android.view.View;

/*
 * An interface which possesses some methods from an {@link Fragment} to be used anywhere without need the Fragment itself.
 */
public interface ISupportFragView {

    /*
     * @see Fragment#getView
     */
    View getView();

    /*
     *@see Fragment#startActivityForResult
     */
    void startActivityForResult(final Intent intent, final int requestCode);

    /**
     * @see Fragment#getActivity()
     */
    FragmentActivity getActivity();

    /**
     * @see Fragment#getActivity()
     */
    ISupportView getSupportView();

    /**
     * @see Fragment#getLoaderManager()
     */
    LoaderManager getLoaderManager();

    /**
     * @see Fragment#startActivity(Intent)
     */
    void startActivity(final Intent intent);

    /**
     * @see Fragment#getString(resId)
     */
    String getString(@StringRes int resId);

    /**
     * @see Fragment#getContext()
     */
    Context getContext();

}
