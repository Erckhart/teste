package br.com.voidapp.fm.view.holder;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.facebook.Profile;
import com.facebook.login.widget.ProfilePictureView;

import br.com.voidapp.fm.R;
import br.com.voidapp.fm.view.ISupportFragView;

public class HolderFragProfile {

    private final ISupportFragView mView;

    private ProfilePictureView mProfilePicture;
    private TextView mUserName;
    private Button mLogoutButton;
    private View mLoadingContainer;

    public HolderFragProfile(final ISupportFragView view) {
        mView = view;
        initializeFields();
    }

    private void initializeFields() {
        mProfilePicture = (ProfilePictureView) mView.getView().findViewById(R.id.frag_profile_picture);
        mUserName = (TextView) mView.getView().findViewById(R.id.frag_profile_name);
        mLogoutButton = (Button) mView.getView().findViewById(R.id.frag_profile_logout);
        mLoadingContainer = mView.getView().findViewById(R.id.frag_profile_loading);
    }

    public void putValues(final Profile profile) {
        if (profile != null) {
            mProfilePicture.setProfileId(profile.getId());
            mUserName.setText(profile.getName());
        }
    }

    public void setLoadingEnabled(final boolean enabled) {
        mLoadingContainer.setVisibility(enabled ? View.VISIBLE : View.GONE);
    }

    public Button getLogoutButton() {
        return mLogoutButton;
    }
}
