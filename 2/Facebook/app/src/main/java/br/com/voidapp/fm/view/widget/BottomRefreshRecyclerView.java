package br.com.voidapp.fm.view.widget;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LayoutAnimationController;

public class BottomRefreshRecyclerView extends RecyclerView {

    private boolean mScrolledToBottom;
    private OnScrollBottomListener mOnScrollBottomListener;

    private final OnScrollListener mOnScrollListener = new OnScrollListener() {
        @Override
        public void onScrollStateChanged(final RecyclerView view, final int scrollState) {
            super.onScrollStateChanged(view, scrollState);
            if(mScrolledToBottom && !view.canScrollVertically(1) && scrollState == SCROLL_STATE_IDLE && mOnScrollBottomListener != null) {
                mOnScrollBottomListener.onReachedBottom(BottomRefreshRecyclerView.this);
            }
        }

        @Override
        public void onScrolled(final RecyclerView recyclerView, final int dx, final int dy) {
            super.onScrolled(recyclerView, dx, dy);
            final int visibleItemCount = recyclerView.getChildCount();
            final int totalItemCount = recyclerView.getLayoutManager().getItemCount();
            final int firstVisibleItem = ((LinearLayoutManager)recyclerView.getLayoutManager()).findFirstVisibleItemPosition();
            final int lastItem = firstVisibleItem + visibleItemCount;
            mScrolledToBottom = lastItem == totalItemCount;
        }
    };

    public BottomRefreshRecyclerView(final Context context) {
        super(context);
    }

    public BottomRefreshRecyclerView(final Context context, final AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public BottomRefreshRecyclerView(final Context context, final AttributeSet attrs, final int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private void init() {
        this.addOnScrollListener(mOnScrollListener);
    }

    public void setOnScrollBottomListener(final OnScrollBottomListener onScrollBottomListener) {
        mOnScrollBottomListener = onScrollBottomListener;
    }

    @Override
    protected void attachLayoutAnimationParameters(final View child, final ViewGroup.LayoutParams params, final int index, final int count) {
        if(getAdapter() == null) {
            super.attachLayoutAnimationParameters(child, params, index, count);
        } else {

            final LayoutAnimationController.AnimationParameters animationParams = params.layoutAnimationParameters;

            animationParams.count = count;
            animationParams.index = index;

        }
    }

    public interface OnScrollBottomListener {
        void onReachedBottom(final View view);
    }

}