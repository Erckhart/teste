package br.com.voidapp.fm.view.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.Arrays;
import java.util.List;

import br.com.voidapp.fm.model.EFbMenu;

public class AdpMenu extends FragmentPagerAdapter {

    private final List<EFbMenu> mMenus;

    public AdpMenu(final FragmentManager fm) {
        super(fm);
        mMenus = Arrays.asList(EFbMenu.values());
    }

    @Override
    public Fragment getItem(final int position) {
        return mMenus.get(position).getView();
    }

    public EFbMenu getMenu(final int position) {
        return mMenus.get(position);
    }


    @Override
    public int getCount() {
        return mMenus.size();
    }

    @Override
    public CharSequence getPageTitle(final int position) {
        return null;
    }



}
