package br.com.voidapp.fm.model;

import com.google.android.gms.maps.model.LatLng;

public class Location {

    private float latitude;
    private float longitude;

    public LatLng getPosition() {
        return new LatLng(latitude,longitude);
    }

}
