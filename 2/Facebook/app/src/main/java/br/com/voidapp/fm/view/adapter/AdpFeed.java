package br.com.voidapp.fm.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import br.com.voidapp.fm.R;
import br.com.voidapp.fm.model.Post;
import br.com.voidapp.fm.view.holder.HolderFeedItem;

public class AdpFeed extends RecyclerView.Adapter<HolderFeedItem> {

    private final LinkedHashMap<String, Post> mPosts;
    private final Context mContext;

    public AdpFeed(final Context context, final List<Post> posts) {
        mContext = context;
        mPosts = new LinkedHashMap<>();
        addItems(posts, false);
    }

    public void addItems(final List<Post> posts) {
        addItems(posts, true);
    }

    private void addItems(final List<Post> posts, final boolean updateView) {
        for (final Post post : posts) {
            mPosts.put(post.getId(), post);
        }
        if (updateView) {
            notifyDataSetChanged();
        }
    }

    public void setItems(final List<Post> posts) {
        mPosts.clear();
        addItems(posts);
    }

    public Post getItem(final int position) {
        return new ArrayList<Post>(mPosts.values()).get(position);
    }

    @Override
    public HolderFeedItem onCreateViewHolder(final ViewGroup parent, final int viewType) {
        final View view = LayoutInflater.from(mContext).inflate(R.layout.frag_feed_item, parent, false);
        return new HolderFeedItem(view);
    }

    @Override
    public void onBindViewHolder(final HolderFeedItem holder, final int position) {
        holder.putValues(getItem(position));
    }

    @Override
    public int getItemCount() {
        return mPosts.size();
    }
}
