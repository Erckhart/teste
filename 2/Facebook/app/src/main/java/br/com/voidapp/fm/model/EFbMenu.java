package br.com.voidapp.fm.model;

import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;

import br.com.voidapp.fm.R;
import br.com.voidapp.fm.interfce.IFbMenu;
import br.com.voidapp.fm.view.fragment.FragFeed;
import br.com.voidapp.fm.view.fragment.FragProfile;

public enum EFbMenu implements IFbMenu {

    FEED(R.drawable.ic_menu_feed_selected, R.drawable.ic_menu_feed_unselected, new FragFeed()) {
        @Override
        public void changeMenu(@NonNull final TabLayout.Tab feedTab, @NonNull final TabLayout.Tab profileTab) {
            feedTab.setIcon(getSelectedIconRes());
            profileTab.setIcon(PROFILE.getUnselectedIconRes());
        }
    },
    PROFILE(R.drawable.ic_menu_user_selected, R.drawable.ic_menu_user_unselected, new FragProfile()) {
        @Override
        public void changeMenu(@NonNull final TabLayout.Tab feedTab, @NonNull final TabLayout.Tab profileTab) {
            feedTab.setIcon(FEED.getUnselectedIconRes());
            profileTab.setIcon(getSelectedIconRes());
        }
    };

    private final int mSelectedIconRes;
    private final int mUnselectedIconRes;
    private final Fragment mView;

    EFbMenu(@DrawableRes final int selectedIconRes, @DrawableRes final int unselectedIconRes, @NonNull final Fragment view) {
        mSelectedIconRes = selectedIconRes;
        mUnselectedIconRes = unselectedIconRes;
        mView = view;
    }

    public int getSelectedIconRes() {
        return mSelectedIconRes;
    }

    public int getUnselectedIconRes() {
        return mUnselectedIconRes;
    }

    public Fragment getView() {
        return mView;
    }
}
