package br.com.voidapp.fm.controller.util;

import com.facebook.FacebookRequestError;

public class ErrorUtil {

    public static String getErrorContent(final FacebookRequestError error) {
        if (error.getErrorCode() == -1) {
            return "Verifique sua conexão de internet.";
        } else {
            return error.getErrorMessage();
        }
    }

}
