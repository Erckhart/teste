package br.com.voidapp.fm.controller;

import android.content.Intent;
import android.os.Handler;
import android.support.design.widget.Snackbar;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;

import br.com.voidapp.fm.controller.social.SocialFacebook;
import br.com.voidapp.fm.view.ISupportView;
import br.com.voidapp.fm.view.activity.ActMain;
import br.com.voidapp.fm.view.holder.HolderActLogin;

public class ControllerActLogin {

    private final Runnable mSplashRunnable = new Runnable() {
        @Override
        public void run() {
            mHolder.setSplashEnabled(false);
            if (mView.getApp().getFacebookManager().isLoggedIn()) {
                mView.startActivity(new Intent(mView.getBaseContext(), ActMain.class));
                mView.finish();
            } else {
                mCallbackManager = CallbackManager.Factory.create();
                mHolder.getLoginButton().registerCallback(mCallbackManager, mFacebookLoginCallback);
            }
        }
    };


    private final FacebookCallback<LoginResult> mFacebookLoginCallback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(final LoginResult loginResult) {
            mView.startActivity(new Intent(mView.getBaseContext(), ActMain.class));
            mView.finish();
        }

        @Override
        public void onCancel() {
            // do nothing
        }

        @Override
        public void onError(final FacebookException error) {
            final Snackbar snackbar = Snackbar.make(mHolder.getLoginButton(), error.toString(), Snackbar.LENGTH_LONG);
            snackbar.show();
        }
    };

    private final HolderActLogin mHolder;
    private final ISupportView mView;
    private final Handler mPostAction;

    private CallbackManager mCallbackManager;


    public ControllerActLogin(final ISupportView view) {
        mView = view;
        mHolder = new HolderActLogin(view);
        mHolder.getLoginButton().setPublishPermissions(SocialFacebook.PUBLISH_PERMISSIONS);
        mPostAction = new Handler();
        mPostAction.postDelayed(mSplashRunnable, 500);
    }

    public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }
}
