package br.com.voidapp.fm.view.holder;

import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;

import br.com.voidapp.fm.R;
import br.com.voidapp.fm.view.ISupportView;
import br.com.voidapp.fm.view.widget.ScrollableFloatingActionButton;

public class HolderActMain {

    private final ISupportView mView;

    private Toolbar mToolbar;
    private ViewPager mViewPager;
    private TabLayout mTabManager;

    private FloatingActionButton mNewPostButton;

    public HolderActMain(final ISupportView view) {
        mView = view;
        initializeFields();
    }

    private void initializeFields() {
        mToolbar = (Toolbar) mView.findViewById(R.id.act_main_toolbar);
        mViewPager = (ViewPager) mView.findViewById(R.id.act_main_pager);
        mTabManager = (TabLayout) mView.findViewById(R.id.act_main_tabs);
        mNewPostButton = (FloatingActionButton) mView.findViewById(R.id.act_main_new_post);
    }

    public TabLayout getTabManager() {
        return mTabManager;
    }

    public Toolbar getToolbar() {
        return mToolbar;
    }

    public ViewPager getViewPager() {
        return mViewPager;
    }

    public FloatingActionButton getNewPostButton() {
        return mNewPostButton;
    }
}
