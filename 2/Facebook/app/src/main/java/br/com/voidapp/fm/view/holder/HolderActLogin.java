package br.com.voidapp.fm.view.holder;

import android.view.View;

import com.facebook.login.widget.LoginButton;

import br.com.voidapp.fm.R;
import br.com.voidapp.fm.view.ISupportView;
import br.com.voidapp.fm.view.widget.SlidingLayout;

public class HolderActLogin {

    private final ISupportView mView;

    private LoginButton mLoginButton;
    private SlidingLayout mSplashLayout;

    public HolderActLogin(final ISupportView view) {
        mView = view;
        initializeFields();
    }

    private void initializeFields() {

        mLoginButton = (LoginButton) mView.findViewById(R.id.act_login_bt_login);
        mSplashLayout = (SlidingLayout) mView.findViewById(R.id.act_login_splash_container);

    }

    public LoginButton getLoginButton() {
        return mLoginButton;
    }

    public void setSplashEnabled(final boolean enabled) {
        mSplashLayout.setVisibility(enabled ? View.VISIBLE : View.GONE);
    }
}
