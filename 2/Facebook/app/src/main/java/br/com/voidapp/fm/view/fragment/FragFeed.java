package br.com.voidapp.fm.view.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import br.com.voidapp.fm.R;
import br.com.voidapp.fm.controller.ControllerFragFeed;
import br.com.voidapp.fm.view.IFragPager;
import br.com.voidapp.fm.view.ISupportView;

public class FragFeed extends Fragment implements IFragPager {

    private ISupportView mSupportView;
    private ControllerFragFeed mController;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.frag_feed, container, false);
    }

    @Override
    public void onViewCreated(final View view, final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mController = new ControllerFragFeed(this);
    }

    @Override
    public void onAttach(final Context context) {
        super.onAttach(context);
        if(context instanceof ISupportView) {
            mSupportView = (ISupportView) context;
        }
    }

    @Override
    public void onAttach(final Activity activity) {
        super.onAttach(activity);
        if(activity instanceof ISupportView) {
            mSupportView = (ISupportView) activity;
        }
    }

    @Override
    public ISupportView getSupportView() {
        return mSupportView;
    }

    @Override
    public void onFragmentSelected() {
        mController.onFragmentSelected();
    }
}
