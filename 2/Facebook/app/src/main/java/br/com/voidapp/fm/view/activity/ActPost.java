package br.com.voidapp.fm.view.activity;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import br.com.voidapp.fm.R;
import br.com.voidapp.fm.controller.ControllerActPost;

public class ActPost extends FbActivity {

    private ControllerActPost mController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_post);
        mController = new ControllerActPost(this);
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        return mController.onOptionsItemSelected(item);
    }
}
