package br.com.voidapp.fm.controller;

import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.Profile;

import java.util.ArrayList;
import java.util.List;

import br.com.voidapp.fm.controller.social.SocialFacebook;
import br.com.voidapp.fm.model.Post;
import br.com.voidapp.fm.view.ISupportFragView;
import br.com.voidapp.fm.view.adapter.AdpFeed;
import br.com.voidapp.fm.view.holder.HolderFragFeed;
import br.com.voidapp.fm.view.widget.BottomRefreshRecyclerView;

public class ControllerFragFeed {

    private final BottomRefreshRecyclerView.OnScrollBottomListener mOnScrollBottomListener = new BottomRefreshRecyclerView.OnScrollBottomListener() {
        @Override
        public void onReachedBottom(final View view) {
            loadFeeds();
        }
    };

    private final SocialFacebook.OnUserTimelineLoadListener mOnUserTimelineLoadListener = new SocialFacebook.OnUserTimelineLoadListener() {
        @Override
        public void onTimelineRetrieved(final String userID, final List<Post> posts, final GraphRequest nextPaging) {
            if(mNextPagingRequest == null && mAdpFeed != null) {
                mAdpFeed.setItems(new ArrayList<Post>());
            }
            mHolder.getSwipeToRefresh().setRefreshing(false);
            if (mAdpFeed == null) {
                mAdpFeed = new AdpFeed(mView.getContext(), posts);
                mHolder.getFeedView().setAdapter(mAdpFeed);
            } else {
                mAdpFeed.addItems(posts);
            }
            mNextPagingRequest = nextPaging;
        }

        @Override
        public void onTimelineError(final String title, final String content) {
            mHolder.getSwipeToRefresh().setRefreshing(false);
            Snackbar.make(mHolder.getFeedView(), content, Snackbar.LENGTH_LONG).show();
        }
    };

    private final SwipeRefreshLayout.OnRefreshListener mOnRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            mNextPagingRequest = null;
            loadFeeds();
        }
    };

    private final ISupportFragView mView;
    private final HolderFragFeed mHolder;
    private final SocialFacebook mFbManager;

    private AdpFeed mAdpFeed;

    private GraphRequest mNextPagingRequest;

    public ControllerFragFeed(final ISupportFragView view) {
        mView = view;
        mHolder = new HolderFragFeed(mView);
        mFbManager = mView.getSupportView().getApp().getFacebookManager();
        initialize();
        loadFeeds();
        initializeActions();
    }

    private void initializeActions() {
        mHolder.getSwipeToRefresh().setOnRefreshListener(mOnRefreshListener);
        mHolder.getFeedView().setOnScrollBottomListener(mOnScrollBottomListener);
    }

    private void initialize() {
        final RecyclerView recyclerView = mHolder.getFeedView();
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(mView.getSupportView().getApplicationContext()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    private void loadFeeds() {
        if (mNextPagingRequest == null) {
            mFbManager.requestUserTimeline(AccessToken.getCurrentAccessToken().getUserId(), mOnUserTimelineLoadListener);
        } else {
            mFbManager.requestUserTimeline(AccessToken.getCurrentAccessToken().getUserId(), mNextPagingRequest, mOnUserTimelineLoadListener);
        }
    }

    public void onFragmentSelected() {
        mHolder.getSwipeToRefresh().setRefreshing(true);
        mOnRefreshListener.onRefresh();
    }
}
